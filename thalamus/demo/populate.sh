#!/bin/bash

# SPDX-FileCopyrightText: 2017-2024 GNU Solidario <health@gnusolidario.org>
# SPDX-FileCopyrightText: 2017-2024 Luis Falcón <falcon@gnuhealth.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

echo "Importing ...."
python3 ./import_pg.py
