#!/usr/bin/env python
# SPDX-FileCopyrightText: 2017-2024 Luis Falcón <falcon@gnuhealth.org>
# SPDX-FileCopyrightText: 2017-2024 GNU Solidario <health@gnusolidario.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

#########################################################################
# Thalamus, the GNU Health Federation Message and Authentication Server #
#                                                                       #
#             Thalamus is part of the GNU Health project                #
#                   https://www.gnuhealth.org                           #
#########################################################################
#                           Thalamus package                            #
#                       __init__.py declaration file                    #
#########################################################################
